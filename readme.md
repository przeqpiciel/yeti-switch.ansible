
# Welcome

Hi, thank you for interres in my ansible playbook. This playbook helps me implement yeti-switch on single machine

## Warranty

I dont guarantee that playbooks will work.
## How to use

First of all you need to configre host_vars, insert values. Because that playbooks also install [FERM](http://ferm.foo-projects.org/) it is important to set some IPs

```
trusted_ssh:
  - { 'ip': '1.2.3.4', 'comment': 'home' }
  - { 'ip': '4.3.2.1', 'comment': 'office' }

trusted_sip:
  - { 'ip': '1.1.1.1', 'comment': 'ISP_01' }
  - { 'ip': '2.2.2.2', 'comment': 'ISP_02' }

routing:
  database_name: db_name_change_it
  database_user: db_user_change_it
  database_password: db_pass_change_it

cdr:
  database_name: cdr_name_change_it
  database_user: cdr_user_change_it
  database_password: cdr_pass_change_it

sip:
  port: 5060
  tcp: false

rtp:
  min: 10000
  max: 20000
  ```
In `trusted_ssh` you declare IPs which are allowed for SSH conection. Here you can put IPs so much as you will.
Section `trusted_sip` is section where you put all IPs which should communicate with your SIP server.
`routing` and `cdr` it is only for databases which will be installed on machine

Now you need configure ansible inventory - `hosts` file.

```
[hosts]
yeti-example	ansible_host=5.6.7.8 ansible_port=22
```
I name it as `yeti-example` and put IP address `5.6.7.8` and SSH are accessible on 22 port. 
Now you can run:

ansible-playbook -i hosts -l yeti-example yeti-routing.yml

ansible should install everything and put yeti-switch up & running